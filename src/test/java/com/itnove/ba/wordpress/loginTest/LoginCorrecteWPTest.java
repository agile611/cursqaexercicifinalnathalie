package com.itnove.ba.wordpress.loginTest;

import com.itnove.ba.BaseWPTest;
import com.itnove.ba.wordpress.pages.DashboardWPPage;
import com.itnove.ba.wordpress.pages.LoginPageWP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginCorrecteWPTest extends BaseWPTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPageWP loginPageWP = new LoginPageWP(driver);
        loginPageWP.loginWP("admin","admin");
        DashboardWPPage dashboardPage = new DashboardWPPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
    }
}
