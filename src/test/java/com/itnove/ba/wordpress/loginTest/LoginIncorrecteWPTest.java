package com.itnove.ba.wordpress.loginTest;

import com.itnove.ba.BaseWPTest;
import com.itnove.ba.wordpress.pages.LoginPageWP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginIncorrecteWPTest extends BaseWPTest {

    public void checkErrors(String user, String passwd){
        LoginPageWP loginPageWP = new LoginPageWP(driver);
        loginPageWP.loginWP(user, passwd);
        assertTrue(loginPageWP.isErrorLoginMessagePresent(driver, wait));

    }

    @Test
    public void testApp() throws InterruptedException {

        checkErrors("admin","admin1");

        checkErrors("admin1","admin");

        checkErrors("admin1","admin1");
    }
}
