package com.itnove.ba.wordpress.loginTest;

import com.itnove.ba.BaseWPTest;
import com.itnove.ba.wordpress.pages.ForgotWPPage;
import org.junit.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ForgotPasswordWPTest extends BaseWPTest {



    @Test
    public void testApp() throws InterruptedException {

        ForgotWPPage forgotWPPage = new ForgotWPPage(driver);
        Assert.assertTrue(forgotWPPage.isSuiteLoginPresent());
        forgotWPPage.clickRememberPass();
        Assert.assertTrue(forgotWPPage.isLostPassworLoaded());
        forgotWPPage.suiteForgotWP("admin");
        assertTrue(forgotWPPage.isMessageError());

    }
}
