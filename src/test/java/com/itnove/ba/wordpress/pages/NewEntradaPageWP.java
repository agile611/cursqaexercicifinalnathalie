package com.itnove.ba.wordpress.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class NewEntradaPageWP {

    private WebDriver driver;

    @FindBy(id = "menu-posts")
    public WebElement postNew;

    @FindBy(id = "title")
    public WebElement titlePostTextBox;

    @FindBy(id = "wp-content-media-buttons")
    public WebElement mediaButton;

    @FindBy(id = "__wp-uploader-id-1")
    public WebElement fileUpdate;

    @FindBy(xpath = ".//*@id=['span__wp-uploader-id-0']/div[5]/div[1]/div[2]/button[1]/font[1]")
    public WebElement saveButton;

    /*@FindBy(id = "mceu_30") id("__wp-uploader-id-0")/div[5]/div[1]/div[2]/button[1]/font[1]
    public WebElement contentTextBox;*///.//*[@class='search_form']"


    @FindBy(id = "publish")
    public WebElement buttonPublish;

    public boolean isNewPostLoaded(WebDriver driver){
        return postNew.isDisplayed();
    }

    public void introducirTitle(String title){
        titlePostTextBox.clear();
        titlePostTextBox.click();
        titlePostTextBox.sendKeys(title);
    }

    public void suiteFileUpdate(){
        mediaButton.click();
    }
    public void browseFile(String name){
        fileUpdate.sendKeys(name);
    }

    public void saveDocument(){
        saveButton.click();
    }



   /* public void introducirContent(String content){
        contentTextBox.click();
        contentTextBox.sendKeys(content);
    }*/

    public void clickOnButtonPublish(){
        buttonPublish.click();

    }
    /*@FindBy(id = "SAVE")
    public WebElement saveButton;

    public void fillName(String name){
        nameTextbox.sendKeys(name);
    }

    public void saveAccount(){
        saveButton.click();


    }
    }*/

    public NewEntradaPageWP(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
