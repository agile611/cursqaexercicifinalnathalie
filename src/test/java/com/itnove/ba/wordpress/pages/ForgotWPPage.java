package com.itnove.ba.wordpress.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class ForgotWPPage {

    private WebDriver driver;

    @FindBy(id = "loginTestOC")
    public WebElement suiteLogin;

    @FindBy(partialLinkText = "Heu perdut la contrasenya?")
    public WebElement buttonForgotWP;

    @FindBy(id = "lostpasswordform")
    public WebElement rememberPassWP;

    @FindBy(id = "user_login")
    public WebElement userLoginTextBox;

    @FindBy(id = "wp-submit")
    public WebElement buttonSubmitWP;

    @FindBy(id = "error-page")
    public WebElement messageError;



    public boolean isSuiteLoginPresent() {
        return suiteLogin.isDisplayed();
    }

    public void clickRememberPass(){
        buttonForgotWP.click();
    }

    public boolean isLostPassworLoaded(){
        return rememberPassWP.isDisplayed();
    }

    public void suiteForgotWP(String user) {
        userLoginTextBox.click();
        userLoginTextBox.sendKeys(user);
        buttonSubmitWP.click();

    }

    public boolean isMessageError(){
        return messageError.isDisplayed();
    }


    public ForgotWPPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    }
