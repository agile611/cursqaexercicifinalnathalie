package com.itnove.ba.wordpress.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPageWP {

    private WebDriver driver;

    @FindBy(id = "user_login")
    public WebElement usernameWPTextBox;

    @FindBy(id = "user_pass")
    public WebElement userpasswordWPTextBox;

    @FindBy(id = "wp-submit")
    public WebElement buttonWPLogin;

    @FindBy(id = "login_error")
    public WebElement errorLoginMessage;


    public void loginWP(String user, String passwd){
        usernameWPTextBox.click();
        usernameWPTextBox.clear();
        usernameWPTextBox.sendKeys(user);
        userpasswordWPTextBox.click();
        userpasswordWPTextBox.clear();
        userpasswordWPTextBox.sendKeys(passwd);
        buttonWPLogin.click();
    }

    public boolean isErrorLoginMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorLoginMessage));
        return errorLoginMessage.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(buttonWPLogin));
        return buttonWPLogin.isDisplayed();
    }

    public String errorMessageDisplayed(){return errorLoginMessage.getText();}

    public LoginPageWP(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
