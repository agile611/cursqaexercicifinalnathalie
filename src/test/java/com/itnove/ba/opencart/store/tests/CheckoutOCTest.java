package com.itnove.ba.opencart.store.tests;

import com.itnove.ba.BaseOCStoreTest;
import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.store.pages.CheckoutPageStoreOC;
import com.itnove.ba.opencart.store.pages.DashboardPageStoreOC;
import com.itnove.ba.opencart.store.pages.SearchResultsOCPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CheckoutOCTest extends BaseOCStoreTest {

    @Test
    public void CheckoutOCTest() throws InterruptedException {
        DashboardPageStoreOC dashboardPageStoreOC = new DashboardPageStoreOC(driver);
        assertTrue(dashboardPageStoreOC.isDashboardYourStoreLoaded());
        dashboardPageStoreOC.clickOnImputSearch("MacBook");
        assertTrue(dashboardPageStoreOC.isListProductsPresent());
        SearchResultsOCPage searchResultsOCPage = new SearchResultsOCPage(driver);
        searchResultsOCPage.clickOnAddCartButton();
        Thread.sleep(8000);
        CheckoutPageStoreOC checkoutPageStoreOC = new CheckoutPageStoreOC(driver);
        checkoutPageStoreOC.clickOnCheckoutButton();
        Thread.sleep(8000);
        checkoutPageStoreOC.clickOnLittleButton();
        assertTrue(checkoutPageStoreOC.isCheckoutButtonPresent());
        checkoutPageStoreOC.clickOnContinueButton();
        Thread.sleep(8000);
        checkoutPageStoreOC.personalDetails("");
        //assertTrue(checkoutPageStoreOC.isPresentClickButto2());
        checkoutPageStoreOC.clickOnButton2();
        Thread.sleep(8000);
        assertTrue(checkoutPageStoreOC.isPresentSteep3Pay(driver, wait));
        Thread.sleep(8000);
        checkoutPageStoreOC.clickOnAccepTerms();
        Thread.sleep(8000);
        assertTrue(checkoutPageStoreOC.isPresentMessageWarning(driver, wait));












    }
}
