package com.itnove.ba.opencart.store.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class DashboardPageStoreOC {

    private WebDriver driver;

    @FindBy(id = "logo")
    public WebElement logoStore;

    @FindBy(xpath = "id(\"searchTest\")/input[1]")
    public WebElement imputSearch;

    @FindBy(xpath = "id('searchTest')/span[1]/button[1]")
    public WebElement buttonLupa;

    @FindBy(xpath = "id('content')/div[3]")
    public WebElement foundProduct;

    @FindBy(xpath = "//*[@id='content']/p[2]")
    public WebElement notFoundProduct;


    public boolean isDashboardYourStoreLoaded(){
        return logoStore.isDisplayed();
    }

    public void clickOnImputSearch(String keyword){
        imputSearch.click();
        imputSearch.clear();
        imputSearch.sendKeys(keyword);
        buttonLupa.click();
    }

    public boolean isListProductsPresent(){
        return foundProduct.isDisplayed();
    }

    public boolean errorMessageNotFoundProduct(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(notFoundProduct));
        return notFoundProduct.isDisplayed();
    }

    public String errorMessageNotFoundProduct(){
        return notFoundProduct.getText();
    }




    public DashboardPageStoreOC(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
