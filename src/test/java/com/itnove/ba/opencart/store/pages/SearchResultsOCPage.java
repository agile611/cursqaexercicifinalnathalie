package com.itnove.ba.opencart.store.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class SearchResultsOCPage {

    private WebDriver driver;

    @FindBy(xpath = "id('content')/div[3]")
    public WebElement listProducts;

    @FindBy(xpath = "id(\"content\")/div[3]/div[1]/div[1]/div[2]/div[2]/button[1]")
    public WebElement addToCartButton;





    public void clickOnAddCartButton() {

        addToCartButton.click();
    }




    public boolean isSearchResultsPageOCLoaded(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(listProducts));
        return listProducts.isDisplayed();
    }

    public SearchResultsOCPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
