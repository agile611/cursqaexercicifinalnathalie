package com.itnove.ba.opencart.store.tests;

import com.itnove.ba.BaseOCStoreTest;
import com.itnove.ba.opencart.store.pages.DashboardPageStoreOC;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchIncorrecteOCTest extends BaseOCStoreTest {

    @Test
    public void searchIncorrecteOCTest() throws InterruptedException {
        DashboardPageStoreOC dashboardPageStoreOC = new DashboardPageStoreOC(driver);
        assertTrue(dashboardPageStoreOC.isDashboardYourStoreLoaded());
        dashboardPageStoreOC.clickOnImputSearch("radios");
        assertTrue(dashboardPageStoreOC.errorMessageNotFoundProduct(driver, wait),
                "Your shopping cart is empty!");


    }
}
