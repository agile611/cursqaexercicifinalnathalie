package com.itnove.ba.opencart.store.tests;

import com.itnove.ba.BaseOCStoreTest;
import com.itnove.ba.opencart.store.pages.DashboardPageStoreOC;
import org.testng.annotations.Test;
//import sun.jvm.hotspot.runtime.Thread;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchCorrecteOCTest extends BaseOCStoreTest {

    @Test
    public void searchCorrecteOCTest() throws InterruptedException {
        DashboardPageStoreOC dashboardPageStoreOC = new DashboardPageStoreOC(driver);
        assertTrue(dashboardPageStoreOC.isDashboardYourStoreLoaded());
        dashboardPageStoreOC.clickOnImputSearch("MacBook");
        assertTrue(dashboardPageStoreOC.isListProductsPresent());



    }
}
