package com.itnove.ba.opencart.store.tests;

import com.itnove.ba.BaseOCStoreTest;
import com.itnove.ba.opencart.store.pages.DashboardPageStoreOC;
import com.itnove.ba.opencart.store.pages.NavigationPageStoreOC;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

//import sun.jvm.hotspot.runtime.Thread;

public class DesktopsOCTest extends BaseOCStoreTest {

    @Test
    public void desktopsOCTest() throws InterruptedException {
        DashboardPageStoreOC dashboardPageStoreOC = new DashboardPageStoreOC(driver);
        assertTrue(dashboardPageStoreOC.isDashboardYourStoreLoaded());
        NavigationPageStoreOC navigationPageStoreOC = new NavigationPageStoreOC(driver);
        navigationPageStoreOC.clickOnPC();
        assertTrue(navigationPageStoreOC.isPCDisplayed());
        navigationPageStoreOC.clickOnMac();
        assertTrue(navigationPageStoreOC.isMacDisplayed());
        navigationPageStoreOC.clickOnAll();
        assertTrue(navigationPageStoreOC.isAllDisplayed());
        navigationPageStoreOC.clickOnLaptops1();
        assertTrue(navigationPageStoreOC.isWindsDisplayed());
        navigationPageStoreOC.clickOnLaptops2();
        assertTrue(navigationPageStoreOC.isShowLaptopsNotesDisplayed());
        navigationPageStoreOC.clickOnComponents();
        assertTrue(navigationPageStoreOC.isShowAllComponents());



    }
}
