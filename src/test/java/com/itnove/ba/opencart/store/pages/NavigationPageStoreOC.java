package com.itnove.ba.opencart.store.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class NavigationPageStoreOC {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id='menu']/div[2]/ul/li[1]/a")
    public WebElement desktopsNavigation;

    @FindBy(xpath = "id('menu')/div[2]/ul[1]/li[1]/div[1]/div[1]/ul[1]/li[1]/a[1]")
    public WebElement submenuPCDesktops;

    @FindBy(xpath = "id('menu')/div[2]/ul[1]/li[1]/div[1]/div[1]/ul[1]/li[2]/a[1]")
    public WebElement submenuMacDesktops;

    @FindBy(xpath = "id('menu')/div[2]/ul[1]/li[1]/div[1]/a[1]")
    public WebElement submenuAllDesktops;

    @FindBy(xpath = "id('product-category')/ul[1]/li[3]/a[1]")
    public WebElement pcDisplayed;

    @FindBy(xpath = "id('product-category')/ul[1]/li[3]/a[1]")
    public WebElement macDisplayed;

    @FindBy(xpath = "id('product-category')/ul[1]/li[2]/a[1]")
    public WebElement allDisplayed;

    @FindBy(xpath = "id('menu')/div[2]/ul[1]/li[2]")
    public WebElement laptopsnMenu;

    @FindBy(xpath = "id('menu')/div[2]/ul[1]/li[2]/div[1]/div[1]/ul[1]/li[1]/a[1]")
    public WebElement windowsLaptopsMenu;

    @FindBy(xpath = "id('product-category')/ul[1]/li[3]/a[1]")
    public WebElement windowsLaptopsDisplayed;

    @FindBy(xpath = "id('menu')/div[2]/ul[1]/li[2]/div[1]/a[1]")
    public WebElement submenuAllLaptopsNote;

    @FindBy(xpath = "//*[@id='content']/div[1]/div[2]/p")
    public WebElement allLaptopsNoteDesplayed;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/a")
    public WebElement submenuComponents;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[1]/a")
    public WebElement submenuComponentsOpcion1;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[2]/a")
    public WebElement submenuComponentsOpcion2;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[3]/a")
    public WebElement submenuComponentsOpcion3;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[4]/a")
    public WebElement submenuComponentsOpcion4;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[5]/a")
    public WebElement submenuComponentsOpcion5;

    @FindBy(xpath = ".//*[@id='menu']/div[2]/ul/li[3]/div/a")
    public WebElement allComponents;

    @FindBy(xpath = ".//*[@id='content']/div/div/ul")
    public WebElement allComponentsLoaded;



    public void clickOnPC (){
        desktopsNavigation.click();
        submenuPCDesktops.click();
    }

    public boolean isPCDisplayed () {
        return pcDisplayed.isDisplayed();
    }


    public void clickOnMac (){
        desktopsNavigation.click();
        submenuMacDesktops.click();
    }

    public boolean isMacDisplayed () {
        return macDisplayed.isDisplayed();
    }

    public void clickOnAll (){
        desktopsNavigation.click();
        submenuAllDesktops.click();
    }

    public boolean isAllDisplayed () {
        return allDisplayed.isDisplayed();

    }

    public void clickOnLaptops1 (){
        laptopsnMenu.click();
        windowsLaptopsMenu.click();

    }

    public boolean isWindsDisplayed () {
        return windowsLaptopsDisplayed.isDisplayed();

    }

    public void clickOnLaptops2 (){
        laptopsnMenu.click();
        submenuAllLaptopsNote.click();

    }


    public boolean isShowLaptopsNotesDisplayed () {
        return allLaptopsNoteDesplayed.isDisplayed();

    }

    public void clickOnComponents(){
        submenuComponents.click();
        submenuComponentsOpcion1.click();
        submenuComponents.click();
        submenuComponentsOpcion2.click();
        submenuComponents.click();
        submenuComponentsOpcion3.click();
        submenuComponents.click();
        submenuComponentsOpcion4.click();
        submenuComponents.click();
        submenuComponentsOpcion5.click();
        submenuComponents.click();
        allComponents.click();
    }

    public boolean isShowAllComponents () {
        return allComponentsLoaded.isDisplayed();
    }


    public NavigationPageStoreOC(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }
    }
