package com.itnove.ba.opencart.store.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CheckoutPageStoreOC {

    private WebDriver driver;

    @FindBy(xpath = "id('top-links')/ul[1]/li[5]/a[1]/span[1]")
    public WebElement buttonCheckout;

    @FindBy(xpath = "id('collapse-checkout-option')/div[1]/div[1]/div[1]/div[2]/label[1]/input[1]")
    public WebElement littleButton;

    @FindBy(xpath = "//*[@id='button-account']")
    public WebElement continueButton;

    @FindBy(xpath = "//*[@id='input-payment-firstname']")
    public WebElement firstName;

    @FindBy(xpath = "//*[@id='input-payment-lastname']")
    public WebElement lastName;

    @FindBy(xpath = "//*[@id='input-payment-email']")
    public WebElement emailImput;

    @FindBy(xpath = "//*[@id='input-payment-telephone']")
    public WebElement telephoneImput;

    @FindBy(xpath = "//*[@id='input-payment-address-1']")
    public WebElement adressImput;

    @FindBy(xpath = "//*[@id='input-payment-city']")
    public WebElement cityImput;

    @FindBy(xpath = "//*[@id='input-payment-postcode']")
    public WebElement postCodeImput;

    @FindBy(xpath = "//*[@id='input-payment-country']")
    public WebElement countryImput;

    @FindBy(xpath = "//*[@id='input-payment-country']/option[209]")
    public WebElement spainImput;

    @FindBy(xpath = "//*[@id='input-payment-zone']")
    public WebElement zoneImput;

    @FindBy(xpath = "//*[@id='input-payment-zone']/option[42]")
    public WebElement santaCruzdeTfeImput;

    @FindBy(xpath = "//*[@id='button-guest']")
    public WebElement continueButton2;

    @FindBy(xpath = "id('accordion')/div[3]/div[1]/h4[1]/a[1]")
    public WebElement steep3Pay;

    @FindBy(xpath = "//*[@id='collapse-payment-method']/div/div/div/input[1]")
    public WebElement agreeTerms;

    @FindBy(id = "button-payment-method")
    public WebElement buttonPay;

    @FindBy(xpath = "//*[@id='collapse-payment-method']/div/div[1]")
    public WebElement messageWarning;

    @FindBy(xpath = "//*[@id='account']/div[3]/div")
    public WebElement messageText;




    public boolean isCheckoutButtonPresent() {

        return buttonCheckout.isDisplayed();
    }

    public void clickOnCheckoutButton() {

        buttonCheckout.click();
    }

    public void clickOnLittleButton() {

        littleButton.click();
    }


    public void clickOnContinueButton() {
        continueButton.click();
    }

    public void personalDetails(String datos ) {
        firstName.click();
        firstName.sendKeys("Nathalie");
        lastName.click();
        lastName.sendKeys("Lozada");
        emailImput.click();
        emailImput.sendKeys("nathalielozada@hotmail.com");
        telephoneImput.click();
        telephoneImput.sendKeys("617917317");
        adressImput.click();
        adressImput.sendKeys("Ripollet");
        cityImput.click();
        cityImput.sendKeys("Barcelona");
        postCodeImput.click();
        postCodeImput.sendKeys("08291");
        countryImput.click();
        spainImput.click();
        zoneImput.click();
        santaCruzdeTfeImput.click();


    }


    public void clickOnButton2 (){
        continueButton2.click();
    }


    public boolean isPresentSteep3Pay(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(steep3Pay));
        return steep3Pay.isDisplayed();
    }



    public void clickOnAccepTerms (){
        agreeTerms.click();
        buttonPay.click();
    }

    public boolean isPresentMessageWarning(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(messageWarning));
        return messageWarning.isDisplayed();
    }


    public void personalDetailsKO(String datos ) {
        firstName.click();
        firstName.sendKeys("Nathalie");
        telephoneImput.click();
        telephoneImput.sendKeys("617917317");
        adressImput.click();
        adressImput.sendKeys("Ripollet");
        cityImput.click();
        cityImput.sendKeys("Barcelona");
        countryImput.click();
        spainImput.click();
    }


    public boolean isMessageTextLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(messageText));
        return messageText.isDisplayed();
    }




    public CheckoutPageStoreOC(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
