package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class AddCategoryOcPage {

    private WebDriver driver;

    @FindBy(xpath = "id('menu-catalog')")
    public WebElement navigationOC;


    @FindBy(xpath = "//*[@id='menu-catalog']/a")
    public WebElement catalogoOC;


    @FindBy(xpath = "//*[@id='collapse1']/li[1]/a")
    public WebElement categoryOC;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/a[1]")
    public WebElement addCategoryNewOC;

    @FindBy(id = "input-name1")
    public WebElement imputCategoryNameOC;

    @FindBy(id = "input-meta-title1")
    public WebElement imputMetaTitleOC;

    @FindBy(xpath = "//*[@id='content']/div[1]/div/div/button")
    public WebElement saveCategory;

    @FindBy(xpath = "id('content')/div[2]/div[1]")
    public WebElement warningErrorMessage;

    @FindBy(xpath = "//*[@id=\"language1\"]/div[3]/div/div")
    public WebElement warningErrorMessageMetaTitle;

    @FindBy(xpath = "//*[@id=\"language1\"]/div[1]/div/div")
    public WebElement warningErrorMessageCategoryName;


    public void navigationButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(navigationOC)
                .click().build().perform();
    }

    public void catalogoButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(catalogoOC)
                .click().build().perform();
    }



    public void categoriesButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(categoryOC)
                .click().build().perform();
    }

    public void addCategoryNewButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(addCategoryNewOC)
                .click().build().perform();
    }

    public void setImputCategoryNameOC (String name){
        imputCategoryNameOC.clear();
        imputCategoryNameOC.click();
        imputCategoryNameOC.sendKeys(name);
    }

    public void setImputMetaTitleOC (String meta){
        imputMetaTitleOC.clear();
        imputMetaTitleOC.click();
        imputMetaTitleOC.sendKeys(meta);
    }

    public void setSaveCategory(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(saveCategory)
                .click().build().perform();
    }

    public boolean isWarningErrorMessage(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(warningErrorMessage));
        return warningErrorMessage.isDisplayed();
    }

    public boolean errorMessageDisplayedCategoryName(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(warningErrorMessageCategoryName));
        return warningErrorMessageCategoryName.isDisplayed();
    }

    public boolean errorMessageDisplayedMetaTitle(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(warningErrorMessageMetaTitle));
        return warningErrorMessageMetaTitle.isDisplayed();
    }


    public String errorMessageDisplayedMetaTitle(){
        return warningErrorMessageMetaTitle.getText();
    }






    public AddCategoryOcPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}


