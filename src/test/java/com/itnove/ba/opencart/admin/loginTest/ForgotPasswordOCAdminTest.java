package com.itnove.ba.opencart.admin.loginTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.ForgotPageOCAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ForgotPasswordOCAdminTest extends BaseOCTest {

    @Test
    public void testApp() throws InterruptedException {

        ForgotPageOCAdmin forgotPageOCAdmin = new ForgotPageOCAdmin(driver);
        forgotPageOCAdmin.clickOnButtonForgot();
        assertTrue(forgotPageOCAdmin.isForgotPasswordPage (driver,wait));

        forgotPageOCAdmin.forgotYourPassword("nathalielozada@gmail.com");
        assertTrue(forgotPageOCAdmin.isErrorMessagePresentPassword (driver,wait));

    }
}
