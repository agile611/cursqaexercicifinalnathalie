package com.itnove.ba.opencart.admin.dashboardTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.AddCategoryOcPage;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class KOAddCategoriesOCTest extends BaseOCTest {



    @Test
    public void testKOAddCategoriesOCCategoryName() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user", "bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver, wait);
        AddCategoryOcPage addCategoryOcPage = new AddCategoryOcPage(driver);
        addCategoryOcPage.navigationButtonClick(hover);
        addCategoryOcPage.catalogoButtonClick(hover);
        addCategoryOcPage.categoriesButtonClick(hover);
        addCategoryOcPage.addCategoryNewButtonClick(hover);
        addCategoryOcPage.setImputMetaTitleOC("solares");
        addCategoryOcPage.setSaveCategory(hover);
        assertTrue(addCategoryOcPage.errorMessageDisplayedCategoryName(driver, wait),
                "Category Name must be between 1 and 255 characters!");

    }

    @Test
    public void testKOAddCategoriesOCMetaTitle() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user", "bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver, wait);
        AddCategoryOcPage addCategoryOcPage = new AddCategoryOcPage(driver);
        addCategoryOcPage.navigationButtonClick(hover);
        addCategoryOcPage.catalogoButtonClick(hover);
        addCategoryOcPage.categoriesButtonClick(hover);
        addCategoryOcPage.addCategoryNewButtonClick(hover);
        addCategoryOcPage.setImputCategoryNameOC("Cargadores");
        addCategoryOcPage.setSaveCategory(hover);
        assertTrue(addCategoryOcPage.errorMessageDisplayedMetaTitle(driver, wait),
                "Meta Title must be greater than 1 and less than 255 characters!");

    }




}
