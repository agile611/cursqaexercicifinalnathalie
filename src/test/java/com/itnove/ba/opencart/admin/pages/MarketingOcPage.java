package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class MarketingOcPage {

    private WebDriver driver;



    @FindBy(id = "menu-marketing")
    public WebElement buttonMarketingOC;




    public void navigationButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(buttonMarketingOC)
                .moveToElement(buttonMarketingOC)
                .click().build().perform();
    }

    public void hoverAndClickEveryMarketing(WebDriver driver, Actions hover) throws InterruptedException {
        navigationButtonClick(hover);
        String listElements = "(.//*[@id='menu-marketing']/ul)/li";//id("menu-marketing")/ul/li
        String lsl = listElements + "/a";
        System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            navigationButtonClick(hover);
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
            hover.moveToElement(buttonMarketingOC)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
        }
    }







    public MarketingOcPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}


