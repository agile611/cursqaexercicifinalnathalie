package com.itnove.ba.opencart.admin.dashboardTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import com.itnove.ba.opencart.admin.pages.MarketingOcPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class MarketingOcAdminTest extends BaseOCTest {

    @Test
    public void MarketingOcAdminTest() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user","bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        assertTrue(dashboardOcPage.isDashboardOcLoaded(driver,wait));
        MarketingOcPage marketingOcPage = new MarketingOcPage(driver);
        marketingOcPage.hoverAndClickEveryMarketing(driver, hover);
        Thread.sleep(6000);

    }
}
