package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class AddProductosOcPage {

    private WebDriver driver;

    @FindBy(xpath = "id('menu-catalog')")
    public WebElement navigationOC;


    @FindBy(xpath = "//*[@id='menu-catalog']/a")
    public WebElement catalogoOC;


    @FindBy(xpath = "id('collapse1')/li[2]/a[1]")
    public WebElement productsOC;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/a[1]")
    public WebElement addProdctNewOC;

    @FindBy(xpath = "//*[@id='input-name1']")
    public WebElement imputProductNameOC;

    @FindBy(xpath = "//*[@id='input-meta-title1']")
    public WebElement imputMetaTitleProductOC;

    @FindBy(xpath = "//*[@id='form-product']/ul/li[2]/a")
    public WebElement dataProductOC;

    @FindBy(id = "input-model")
    public WebElement modelProductOC;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/button[1]")
    public WebElement saveProduct;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]")
    public WebElement messageProductLoaded;

    @FindBy(xpath = "//*[@id='language1']/div[1]/div/div")
    public WebElement productNameError;

    @FindBy(xpath = "id('content')/div[2]/div[1]")
    public WebElement productMetaTitleError;

    @FindBy(xpath = "//*[@id='tab-data']/div[1]/div/div")
    public WebElement productModelError;





    public void navigationButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(navigationOC)
                .click().build().perform();
    }

    public void catalogoButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(catalogoOC)
                .click().build().perform();
    }


    public void productsButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(productsOC)
                .click();
    }

    public void addProductsNewButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(addProdctNewOC)
                .click().build().perform();
    }

    public void setImputProductNameOC (String name){
        imputProductNameOC.clear();
        imputProductNameOC.click();
        imputProductNameOC.sendKeys(name);
    }

    public void setImputMetaTitleProductOC (String meta){
        imputMetaTitleProductOC.clear();
        imputMetaTitleProductOC.click();
        imputMetaTitleProductOC.sendKeys(meta);
    }

    public void setDataProductOC(Actions hover) throws InterruptedException {
        hover.moveToElement(navigationOC)
                .moveToElement(dataProductOC)
                .click().build().perform();
    }

    public void setModelProductOC (String name){
        modelProductOC.clear();
        modelProductOC.click();
        modelProductOC.sendKeys(name);
    }

    public void setSaveProduct(Actions hover) throws InterruptedException {
        hover.moveToElement(dataProductOC)
                .moveToElement(saveProduct)
                .click().build().perform();
    }

    public String isMessageProductAlert(){
        return messageProductLoaded.getText();
    }



    public boolean isMessageProductLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(messageProductLoaded));
        return messageProductLoaded.isDisplayed();
    }

    public String productNameMessageError(String s){
        return productNameError.getText();
    }

    public String productMetaTitleMessageError(String s){
        return productMetaTitleError.getText();
    }

    public String productModelMessageError(String s){
        return productModelError.getText();
    }

    public boolean isMessageProductNameError(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(productNameError));
        return productNameError.isDisplayed();

    }








    public AddProductosOcPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}


