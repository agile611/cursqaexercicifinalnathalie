package com.itnove.ba.opencart.admin.dashboardTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import com.itnove.ba.opencart.admin.pages.SalesOcPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SalesOcAdminTest extends BaseOCTest {

    @Test
    public void SalesOcAdminTest() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user","bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        assertTrue(dashboardOcPage.isDashboardOcLoaded(driver,wait));
        SalesOcPage salesOcPage = new SalesOcPage(driver);
        salesOcPage.hoverAndClickEverySales(driver, hover);
        Thread.sleep(6000);

    }
}
