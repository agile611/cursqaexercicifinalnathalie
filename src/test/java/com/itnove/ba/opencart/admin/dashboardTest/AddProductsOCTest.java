package com.itnove.ba.opencart.admin.dashboardTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.AddProductosOcPage;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class AddProductsOCTest extends BaseOCTest {

    @Test
    public void testAddCategoriesOC() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user", "bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver, wait);
        AddProductosOcPage addProductosOcPage = new AddProductosOcPage(driver);
        addProductosOcPage.navigationButtonClick(hover);
        addProductosOcPage.catalogoButtonClick(hover);
        addProductosOcPage.productsButtonClick(hover);

        addProductosOcPage.addProductsNewButtonClick(hover);
        addProductosOcPage.setImputProductNameOC("Innovmotion1");
        addProductosOcPage.setImputMetaTitleProductOC("Windows");
        addProductosOcPage.setDataProductOC(hover);
        addProductosOcPage.setModelProductOC("Instrument");
        addProductosOcPage.setSaveProduct(hover);
        Thread.sleep(8000);

        assertTrue  (addProductosOcPage.isMessageProductLoaded(driver,wait));
        Thread.sleep(8000);

    }

}
