package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class ForgotPageOCAdmin {

    private WebDriver driver;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/span[1]/a[1]")
    public WebElement buttonForgot;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[1]/h1[1]")
    public WebElement forgotPasswordPage;

    @FindBy(id = "input-email")
    public WebElement imputEmail;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/button[1]")
    public WebElement buttonReset;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/a[1]")
    public WebElement buttonCancel;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[1]/h1[1]")
    public WebElement dashboardOC;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]")
    public WebElement messageNoFoundMail;


    public void clickOnButtonForgot() {
        buttonForgot.click();
    }

    public boolean isForgotPasswordPage(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(forgotPasswordPage));
        return forgotPasswordPage.isDisplayed();
    }

    public void forgotYourPassword(String mail){
        imputEmail.click();
        imputEmail.sendKeys(mail);
        buttonReset.click();
    }

    public boolean isErrorMessagePresentPassword(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(messageNoFoundMail));
        return messageNoFoundMail.isDisplayed();
    }

    public void clicButtonCancel(){
        buttonCancel.click();
    }

    public boolean cancelActionForgotPassword(WebDriver driver, WebDriverWait wait){
        wait.until((ExpectedConditions.visibilityOf(dashboardOC)));
        return dashboardOC.isDisplayed();
    }


    public ForgotPageOCAdmin(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    }
