package com.itnove.ba.opencart.admin.loginTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginIncorrecteOcTest extends BaseOCTest {

    public void checkErrors(String user, String passwd){

        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("","");
        assertTrue(loginPageOcAdmin.isErrorMessagePresent(driver, wait),
               "No match for Username and/or Password.");
    }

    @Test
    public void testApp() throws InterruptedException {
        checkErrors("user","nami");
        checkErrors("resu","bitnami1");
        checkErrors("resu","nami");
    }
}
