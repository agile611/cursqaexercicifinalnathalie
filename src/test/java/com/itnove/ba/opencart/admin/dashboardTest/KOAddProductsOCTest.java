package com.itnove.ba.opencart.admin.dashboardTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.AddProductosOcPage;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class KOAddProductsOCTest extends BaseOCTest {



    @Test
    public void testKOAddProductsOCName() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user", "bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver, wait);
        AddProductosOcPage addProductosOcPage = new AddProductosOcPage(driver);
        addProductosOcPage.navigationButtonClick(hover);
        addProductosOcPage.catalogoButtonClick(hover);
        addProductosOcPage.productsButtonClick(hover);
        addProductosOcPage.addProductsNewButtonClick(hover);
        addProductosOcPage.setImputMetaTitleProductOC("Xoxoxox");
        addProductosOcPage.setDataProductOC(hover);
        addProductosOcPage.setModelProductOC("xoxxoxo");
        addProductosOcPage.setSaveProduct(hover);
        assertTrue(addProductosOcPage.isMessageProductNameError(driver, wait),
                "Warning: Please check the form carefully for errors!");

    }

    @Test
    public void testKOAddProductsOCMetaTitle() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user", "bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver, wait);
        AddProductosOcPage addProductosOcPage = new AddProductosOcPage(driver);
        addProductosOcPage.navigationButtonClick(hover);
        addProductosOcPage.catalogoButtonClick(hover);
        addProductosOcPage.productsButtonClick(hover);
        addProductosOcPage.addProductsNewButtonClick(hover);
        addProductosOcPage.setImputProductNameOC("Xoxoxox");
        addProductosOcPage.setDataProductOC(hover);
        addProductosOcPage.setModelProductOC("xoxxoxo");
        addProductosOcPage.setSaveProduct(hover);
        addProductosOcPage.productMetaTitleMessageError(
                "Warning: Please check the form carefully for errors!");

    }

    @Test
    public void testKOAddProductsOCModel() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user", "bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver, wait);
        AddProductosOcPage addProductosOcPage = new AddProductosOcPage(driver);
        addProductosOcPage.navigationButtonClick(hover);
        addProductosOcPage.catalogoButtonClick(hover);
        addProductosOcPage.productsButtonClick(hover);
        addProductosOcPage.addProductsNewButtonClick(hover);
        addProductosOcPage.setImputProductNameOC("Xoxoxox");
        addProductosOcPage.setImputMetaTitleProductOC("Xoxoxox");
        addProductosOcPage.setSaveProduct(hover);
        addProductosOcPage.productMetaTitleMessageError(
                "Warning: Please check the form carefully for errors!");
    }




}
