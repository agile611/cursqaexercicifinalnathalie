package com.itnove.ba.opencart.admin.dashboardTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.AddCategoryOcPage;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.DeleteCategoryOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DeleteCategoriesOCTest extends BaseOCTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user","bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver,wait);
        AddCategoryOcPage addCategoryOcPage = new AddCategoryOcPage(driver);
        addCategoryOcPage.navigationButtonClick(hover);
        addCategoryOcPage.catalogoButtonClick(hover);
        addCategoryOcPage.categoriesButtonClick(hover);
        DeleteCategoryOcPage deleteCategoryOcPage = new DeleteCategoryOcPage(driver);
        deleteCategoryOcPage.setImputCategoryOC();
        Thread.sleep(8000);
        deleteCategoryOcPage.setEliminarCategoryOC();
        deleteCategoryOcPage.acceptEliminarAlert();
        Thread.sleep(8000);



     }
}
