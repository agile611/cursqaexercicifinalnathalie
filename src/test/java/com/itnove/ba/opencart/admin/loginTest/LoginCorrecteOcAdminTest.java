package com.itnove.ba.opencart.admin.loginTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginCorrecteOcAdminTest extends BaseOCTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPageOcAdmin loginPageOcAdmin = new LoginPageOcAdmin(driver);
        loginPageOcAdmin.loginAdminOC("user","bitnami1");
        DashboardOcPage dashboardOcPage = new DashboardOcPage(driver);
        dashboardOcPage.isDashboardOcLoaded(driver,wait);
        assertTrue(dashboardOcPage.isDashboardOcLoaded(driver,wait));
    }
}
