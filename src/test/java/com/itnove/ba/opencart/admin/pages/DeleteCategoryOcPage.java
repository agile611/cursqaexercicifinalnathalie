package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class DeleteCategoryOcPage {

    private WebDriver driver;

    @FindBy(xpath = "id('form-category')/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]")
    public WebElement seleccionarCategoryOC;

    @FindBy(xpath = "//*[@id='content']/div[1]/div/div/button")
    public WebElement eliminarCategoryOC;

    @FindBy(xpath = "id('content')/div[2]/div[1]")
    public WebElement deleteCatgoryMessage;




    public void setImputCategoryOC(){ seleccionarCategoryOC.click();}

    public void setEliminarCategoryOC(){ eliminarCategoryOC.click();}


    public void acceptEliminarAlert() {
        try {
            WebDriverWait wait = new WebDriverWait (driver, 2);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {
        }
    }










    public DeleteCategoryOcPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}


