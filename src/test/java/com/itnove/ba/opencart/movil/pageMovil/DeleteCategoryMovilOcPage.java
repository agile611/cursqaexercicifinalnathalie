package com.itnove.ba.opencart.movil.pageMovil;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeleteCategoryMovilOcPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='content']/div[1]/div/div/button")
    public WebElement eliminarCategoria;

    @FindBy(xpath = ".//*[@id='content']/div[2]/div[1]")
    public WebElement messageDeletedCategory;


    public void setEliminarCategoria(){
        eliminarCategoria.click();}



    public void acceptEliminarAccountAlert(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {
        }
    }

    public boolean isCategoryDeleted(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(messageDeletedCategory));
        return messageDeletedCategory.isDisplayed();
    }


    public DeleteCategoryMovilOcPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}


