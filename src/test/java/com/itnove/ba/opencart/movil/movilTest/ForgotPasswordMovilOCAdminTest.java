package com.itnove.ba.opencart.movil.movilTest;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.ForgotPageOCAdmin;
import com.itnove.ba.opencart.movil.pageMovil.ForgotPageMovilOCAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ForgotPasswordMovilOCAdminTest extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.get("http://opencart.votarem.lu/admin");
        ForgotPageMovilOCAdmin forgotPageMovilOCAdmin = new ForgotPageMovilOCAdmin(driver);
        forgotPageMovilOCAdmin.clickOnButtonForgot();
        assertTrue(forgotPageMovilOCAdmin.isForgotPasswordPage (driver,wait));

        forgotPageMovilOCAdmin.forgotYourPassword("nathalielozada@gmail.com");
        assertTrue(forgotPageMovilOCAdmin.isErrorMessagePresentPassword (driver,wait));

    }
}
