package com.itnove.ba.opencart.movil.pageMovil;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPageMovilOcAdmin {

    private WebDriver driver;

    @FindBy(id = "input-username")
    public WebElement usernameImput;

    @FindBy(id = "input-password")
    public WebElement userpasswordImput;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/button[1]")
    public WebElement buttonLoginOc;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]")
    public WebElement errorMessageOC;



    public void loginAdminOCMovil(String user, String passwd) {
        usernameImput.clear();
        usernameImput.click();
        usernameImput.sendKeys(user);
        userpasswordImput.clear();
        userpasswordImput.click();
        userpasswordImput.sendKeys(passwd);
        buttonLoginOc.click();
    }

    public boolean isErrorMessagePresentMovil(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOC));
        return errorMessageOC.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(buttonLoginOc));
        return buttonLoginOc.isDisplayed();

    }

    public LoginPageMovilOcAdmin(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }


}
