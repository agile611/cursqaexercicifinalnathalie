package com.itnove.ba.opencart.movil.movilTest;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import com.itnove.ba.opencart.movil.pageMovil.DashboardOcPageMovil;
import com.itnove.ba.opencart.movil.pageMovil.LoginPageMovilOcAdmin;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class LoginMovilIncorrecteOcTest extends BaseSauceBrowserTest {


    @Test
    public void checkErrors(String user, String passwd){
        String nameAccount = UUID.randomUUID().toString();
        String psswAccount = UUID.randomUUID().toString();

        driver.get("http://opencart.votarem.lu/admin");
        LoginPageMovilOcAdmin loginPageMovilOcAdmin = new LoginPageMovilOcAdmin(driver);
        loginPageMovilOcAdmin.loginAdminOCMovil(nameAccount, psswAccount);
        assertTrue(loginPageMovilOcAdmin.isErrorMessagePresentMovil(driver, wait),
               "No match for Username and/or Password.");
    }
}
