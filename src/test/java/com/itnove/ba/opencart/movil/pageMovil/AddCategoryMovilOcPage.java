package com.itnove.ba.opencart.movil.pageMovil;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class AddCategoryMovilOcPage {

    private WebDriver driver;

    @FindBy(xpath = "id('button-menu')")
    public WebElement buttonMenu;

    @FindBy(xpath = "id('menu-catalog')")
    public WebElement navigationOC;


    @FindBy(xpath = "//*[@id='menu-catalog']/a")
    public WebElement catalogoOC;


    @FindBy(xpath = "id('collapse1')/li[1]/a[1]")
    public WebElement categoryOC;

    @FindBy(xpath = "(.//*[@id='content']/div[1]/div/div/a[1])")
    public WebElement addCategoryNewOC;

    @FindBy(id = "input-name1")
    public WebElement imputCategoryNameOC;

    @FindBy(id = "input-meta-title1")
    public WebElement imputMetaTitleOC;

    @FindBy(xpath = "//*[@id='content']/div[1]/div[1]/div[1]/button[1]")
    public WebElement saveCategory;

    @FindBy(xpath = "id('content')/div[2]/div[1]")
    public WebElement successMessage;

    @FindBy(xpath = "//*[@id=\"language1\"]/div[3]/div/div")
    public WebElement warningErrorMessageMetaTitle;

    @FindBy(xpath = "//*[@id=\"language1\"]/div[1]/div/div")
    public WebElement warningErrorMessageCategoryName;


    public void navigationButtonClick(String nameAccount, String metaTiltle)  {
        buttonMenu.click();
        catalogoOC.click();
        categoryOC.click();
        addCategoryNewOC.click();
        imputCategoryNameOC.click();
        imputCategoryNameOC.sendKeys(nameAccount);
        imputMetaTitleOC.click();
        imputMetaTitleOC.sendKeys(metaTiltle);
        saveCategory.click();

    }

    public boolean isCategorySaved(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(successMessage));
        return successMessage.isDisplayed();
    }





    public AddCategoryMovilOcPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}


