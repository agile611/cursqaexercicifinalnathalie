package com.itnove.ba.opencart.movil.movilTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.AddCategoryOcPage;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.DeleteCategoryOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import com.itnove.ba.opencart.movil.pageMovil.AddCategoryMovilOcPage;
import com.itnove.ba.opencart.movil.pageMovil.DashboardOcPageMovil;
import com.itnove.ba.opencart.movil.pageMovil.DeleteCategoryMovilOcPage;
import com.itnove.ba.opencart.movil.pageMovil.LoginPageMovilOcAdmin;
import org.testng.annotations.Test;

import java.util.UUID;

public class DeleteCategoriesMovilOCTest extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        String nameAccount = UUID.randomUUID().toString();
        String metaTitle = UUID.randomUUID().toString();

        driver.get("http://opencart.votarem.lu/admin");
        LoginPageMovilOcAdmin loginPageMovilOcAdmin = new LoginPageMovilOcAdmin(driver);
        loginPageMovilOcAdmin.loginAdminOCMovil("user","bitnami1");
        DashboardOcPageMovil dashboardOcPageMovil = new DashboardOcPageMovil(driver);
        dashboardOcPageMovil.isDashboardOcMOvilLoaded(driver,wait);
        AddCategoryMovilOcPage addCategoryMovilOcPage = new AddCategoryMovilOcPage(driver);
        addCategoryMovilOcPage.navigationButtonClick(nameAccount, metaTitle);
        DeleteCategoryMovilOcPage deleteCategoryMovilOcPage = new DeleteCategoryMovilOcPage(driver);
        deleteCategoryMovilOcPage.setEliminarCategoria();
        deleteCategoryMovilOcPage.acceptEliminarAccountAlert(driver, wait);
        deleteCategoryMovilOcPage.isCategoryDeleted(driver, wait);





     }
}
