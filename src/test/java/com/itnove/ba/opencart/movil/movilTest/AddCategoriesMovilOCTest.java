package com.itnove.ba.opencart.movil.movilTest;

import com.itnove.ba.BaseOCTest;
import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.AddCategoryOcPage;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import com.itnove.ba.opencart.movil.pageMovil.AddCategoryMovilOcPage;
import com.itnove.ba.opencart.movil.pageMovil.DashboardOcPageMovil;
import com.itnove.ba.opencart.movil.pageMovil.LoginPageMovilOcAdmin;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class AddCategoriesMovilOCTest extends BaseSauceBrowserTest {

    @Test
    public void testAddCategoriesOC() throws InterruptedException {
        String nameAccount = UUID.randomUUID().toString();
        String metaTitle = UUID.randomUUID().toString();

        driver.get("http://opencart.votarem.lu/admin");
        LoginPageMovilOcAdmin loginPageMovilOcAdmin = new LoginPageMovilOcAdmin(driver);
        loginPageMovilOcAdmin.loginAdminOCMovil("user","bitnami1");
        DashboardOcPageMovil dashboardOcPageMovil = new DashboardOcPageMovil(driver);
        dashboardOcPageMovil.isDashboardOcMOvilLoaded(driver,wait);
        AddCategoryMovilOcPage addCategoryMovilOcPage = new AddCategoryMovilOcPage(driver);
        addCategoryMovilOcPage.navigationButtonClick(nameAccount, metaTitle);
        assertTrue(addCategoryMovilOcPage.isCategorySaved(driver, wait));
        Thread.sleep(8000);

    }

}
