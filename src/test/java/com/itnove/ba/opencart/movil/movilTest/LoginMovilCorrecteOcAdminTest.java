package com.itnove.ba.opencart.movil.movilTest;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.DashboardOcPage;
import com.itnove.ba.opencart.admin.pages.LoginPageOcAdmin;
import com.itnove.ba.opencart.movil.pageMovil.DashboardOcPageMovil;
import com.itnove.ba.opencart.movil.pageMovil.LoginPageMovilOcAdmin;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginMovilCorrecteOcAdminTest extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.get("http://opencart.votarem.lu/admin");
        LoginPageMovilOcAdmin loginPageMovilOcAdmin = new LoginPageMovilOcAdmin(driver);
        loginPageMovilOcAdmin.loginAdminOCMovil("user","bitnami1");
        DashboardOcPageMovil dashboardOcPageMovil = new DashboardOcPageMovil(driver);
        dashboardOcPageMovil.isDashboardOcMOvilLoaded(driver,wait);
        assertTrue(dashboardOcPageMovil.isDashboardOcMOvilLoaded(driver,wait));
    }
}
