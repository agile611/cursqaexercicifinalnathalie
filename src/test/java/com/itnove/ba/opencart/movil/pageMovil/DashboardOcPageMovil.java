package com.itnove.ba.opencart.movil.pageMovil;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DashboardOcPageMovil {

    private WebDriver driver;

    @FindBy(xpath = "id('content')/div[1]/div[1]/h1[1]")
    public WebElement dashboardOC;


    public boolean isDashboardOcMOvilLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(dashboardOC));
        return dashboardOC.isDisplayed();
    }

    public DashboardOcPageMovil(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}

