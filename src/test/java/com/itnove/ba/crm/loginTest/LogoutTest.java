package com.itnove.ba.crm.loginTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LogoutTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.clickOnIcon();
        dashboardPage.clickOnLogout();
        assertTrue(dashboardPage.isSuiteLoginLoaded());
    }
}
