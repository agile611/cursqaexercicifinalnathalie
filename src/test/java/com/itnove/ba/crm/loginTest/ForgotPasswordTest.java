package com.itnove.ba.crm.loginTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.ForgotPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ForgotPasswordTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        ForgotPage forgotPage = new ForgotPage(driver);
        forgotPage.forgot("user","nathalielozada@gmail.com");
        assertTrue(forgotPage.isErrorMessagePresent(driver, wait));
        assertEquals(forgotPage.errorMessageDisplayed(),
                "Provide both a User Name and an Email Address.");

    }
}
