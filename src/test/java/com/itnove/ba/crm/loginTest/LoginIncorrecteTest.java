package com.itnove.ba.crm.loginTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

public class LoginIncorrecteTest extends BaseTest {

    public void checkErrors(String user, String passwd){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(user, passwd);
        assertTrue(loginPage.isErrorMessagePresent(driver, wait));
        assertEquals(loginPage.errorMessageDisplayed(),
                "You must specify a valid username and password.");
    }

    @Test
    public void testApp() throws InterruptedException {

        checkErrors("user","nami");
        checkErrors("resu","bitnami");
        checkErrors("resu","nami");
    }
}
