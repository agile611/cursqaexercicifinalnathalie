package com.itnove.ba.crm.loginTest;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginCorrecteTest extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
    }
}
