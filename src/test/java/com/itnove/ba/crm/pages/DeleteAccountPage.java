package com.itnove.ba.crm.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DeleteAccountPage {

    private WebDriver driver;


    @FindBy(xpath = "(.//*[@id='tab-actions']/a)")
    public WebElement buttonAction;

    @FindBy(id = "delete_button")
    public WebElement deleteButton;

    @FindBy(xpath = "(.//*[@id='pagination']/td/table/tbody/tr/td[1])[2]")
    public WebElement dashboardAccount;


    public void clickOnButtonAction() {
        buttonAction.click();
    }

    public void clickOnButtonDeleteAccount() {
        deleteButton.click();
    }


    public void acceptEliminarAccountAlert(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
            driver.switchTo().defaultContent();
        } catch (Exception e) {
            Thread.sleep(5000);
        }
    }

    public boolean isPageAccountLoaded(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOf(dashboardAccount));
        return dashboardAccount.isDisplayed();

    }


    public DeleteAccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
