package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CreateAccountPage {

    private WebDriver driver;

    @FindBy (id = "name")
    public WebElement imputName;

    @FindBy (id = "website")
    public WebElement imputwebSite;

    @FindBy (id = "Accounts0emailAddress0")
    public WebElement imputEmailAdress;

    @FindBy (id = "billing_address_street")
    public WebElement imputStreet;

    @FindBy (id = "billing_address_city")
    public WebElement imputCity;

    @FindBy (id = "billing_address_state")
    public WebElement imputState;

    @FindBy (id = "billing_address_postalcode")
    public WebElement imputPostalCode;

    @FindBy (id = "billing_address_country")
    public WebElement imputCountry;

    @FindBy (xpath = "(.//*[@id='SAVE'])[2]")
    public WebElement buttonSave;

    @FindBy (xpath = "(.//*[@id='searchbutton'])[3]")
    public WebElement lupa;

    @FindBy (xpath = "(.//*[@id='query_string'])[5]")
    public WebElement searchAccount;

    @FindBy (xpath = ".//*[@id='recentlyViewedSidebar']/ul/div[1]/li/a")
    public WebElement recentlyViewed;




    public void setImputName (String name, String website, String mail, String state, String city, String street, String postalCode, String country ) {
        imputName.click();
        imputName.sendKeys (name);
        imputwebSite.sendKeys(website);
        imputEmailAdress.sendKeys(mail);
        imputState.sendKeys(state);
        imputCity.sendKeys(city);
        imputStreet.sendKeys(street);
        imputPostalCode.sendKeys(postalCode);
        imputCountry.sendKeys(country);
        buttonSave.click();

    }

    public void  setSearchAccount (String name){
        //searchAccount.click();
        lupa.click();
        searchAccount.sendKeys(name);
        lupa.click();

    }


    public boolean isAccountRecentlyLoaded() {

        return recentlyViewed.isDisplayed();

    }








    public CreateAccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
