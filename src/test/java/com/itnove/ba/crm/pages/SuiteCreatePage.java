package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class SuiteCreatePage {

    private WebDriver driver;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/a)[3]")
    public WebElement createSuite;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[1]/a)[3]")
    public WebElement createAccount;

    @FindBy(xpath = "(.//*[@id='moduleTab_Accounts'])[2]")
    public WebElement createAccountLoaded;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[2]/a)[3]")
    public WebElement createContact;

    @FindBy(xpath = "(.//*[@id='moduleTab_Contacts'])[2]")
    public WebElement createContactLoaded;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[3]/a)[3]")
    public WebElement createOpportunity;

    @FindBy(xpath = "(.//*[@id='moduleTab_Opportunities'])[2]")
    public WebElement createOpportunityLoaded;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[4]/a)[3]")
    public WebElement createLeads;

    @FindBy(xpath = "(.//*[@id='moduleTab_Leads'])[2]")
    public WebElement createLeadsLoaded;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[5]/a)[3]")
    public WebElement createDocument;

    @FindBy(xpath = "(.//*[@id='moduleTab_Documents'])[2]")
    public WebElement createDocumentLoaded;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[6]/a)[3]")
    public WebElement createCall;

    @FindBy(xpath = "(.//*[@id='moduleTab_Calls'])[2]")
    public WebElement createCallLoaded;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul/li[7]/a)[3]")
    public WebElement createTask;

    @FindBy(xpath = "(.//*[@id='moduleTab_Tasks'])[2]")
    public WebElement createTaskLoaded;



    public void clickOnButtonCreateAccount (){
        createSuite.click();
        createAccount.click();
    }

    public boolean isCreateAccountLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(createAccountLoaded));
        return createAccountLoaded.isDisplayed();
    }

    public void clickOnButtonCreateContact (){
        createSuite.click();
        createContact.click();

    }

    public boolean isCreateContactLoaded() {
        return createContactLoaded.isDisplayed();
    }

    public void clickOnButtonCreateOpportunity (){
        createSuite.click();
        createOpportunity.click();

    }

    public boolean isCreateOpportunityLoaded() {
        return createOpportunityLoaded.isDisplayed();
    }


    public void clickOnButtonCreateLead (){
        createSuite.click();
        createLeads.click();

    }

    public boolean isCreateLeadLoaded() {
        return createLeadsLoaded.isDisplayed();
    }

    public void clickOnButtonCreateDocument (){
        createSuite.click();
        createDocument.click();

    }

    public boolean isCreateDocumentLoaded() {
        return createDocumentLoaded.isDisplayed();
    }

    public void clickOnButtonCreateLogCall (){
        createSuite.click();
        createCall.click();

    }

    public boolean isCreateLogCallLoaded() {
        return createCallLoaded.isDisplayed();
    }

    public void clickOnButtonCreateTask (){
        createSuite.click();
        createTask.click();

    }

    public boolean isCreatetaskLoaded() {
        return createTaskLoaded.isDisplayed();
    }





    public SuiteCreatePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
