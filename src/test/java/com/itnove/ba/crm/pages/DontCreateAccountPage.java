package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class DontCreateAccountPage {

    private WebDriver driver;

    @FindBy (id = "name")
    public WebElement imputName;

    @FindBy (id = "website")
    public WebElement imputwebSite;

    @FindBy (id = "Accounts0emailAddress0")
    public WebElement imputEmailAdress;

    @FindBy (id = "billing_address_street")
    public WebElement imputStreet;

    @FindBy (id = "billing_address_city")
    public WebElement imputCity;

    @FindBy (id = "billing_address_state")
    public WebElement imputState;

    @FindBy (id = "billing_address_postalcode")
    public WebElement imputPostalCode;

    @FindBy (id = "billing_address_country")
    public WebElement imputCountry;

    @FindBy (xpath = "(.//*[@id='SAVE'])[2]")
    public WebElement buttonSave;

    @FindBy (xpath = "(.//*[@id='searchbutton'])[3]")
    public WebElement lupa;

    @FindBy (xpath = "(.//*[@id='query_string'])[5]")
    public WebElement searchAccount;

    @FindBy (xpath = ".//*[@id='detailpanel_-1']/div/div/div[1]/div[2]/div")
    public WebElement createdError;




    public void setImputName (String website) {
        imputwebSite.sendKeys(website);
        buttonSave.click();

    }

    public boolean isErrorPresent() {

        return createdError.isDisplayed();

    }








    public DontCreateAccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
