package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class ForgotPage {

    private WebDriver driver;

    @FindBy(id = "forgotpasslink")
    public WebElement buttonForgot;

    @FindBy(id = "fp_user_name")
    public WebElement userTextBox;

    @FindBy(id = "fp_user_mail")
    public WebElement mailTextBox;

    @FindBy(id = "generate_pwd_button")
    public WebElement buttonSubmit;

    @FindBy(xpath = "//*[@id='generate_success']")
    public WebElement generateSuccess;


    public void forgot(String user, String mail){
        buttonForgot.click();
        userTextBox.sendKeys(user);
        mailTextBox.sendKeys();
        buttonSubmit.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(generateSuccess));
        return generateSuccess.isDisplayed();
    }

    public boolean isSubmitButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(buttonSubmit));
        return buttonSubmit.isDisplayed();
    }

    public String errorMessageDisplayed(){
        return generateSuccess.getText();
    }

    public ForgotPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    }
