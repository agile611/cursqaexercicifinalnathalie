package com.itnove.ba.crm.dashboardTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class DeleteCreateAccountTest extends BaseTest {

    @Test
    public void deleteCreateAccountTest() throws InterruptedException {
        driver.get("http://crm.votarem.lu");
        String nameAccount = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        SuiteCreatePage suiteCreatePage = new SuiteCreatePage(driver);
        suiteCreatePage.clickOnButtonCreateAccount();
        assertTrue(suiteCreatePage.isCreateAccountLoaded(driver,wait));
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.setImputName(nameAccount, "http://sdfsdfjsdf.com/", "lalala@gmail.com",
                "Barcelona", "Barcelona", "Alcantara", "38530", "Spain");
        DeleteAccountPage deleteAccountPage = new DeleteAccountPage(driver);
        deleteAccountPage.clickOnButtonAction();
        deleteAccountPage.clickOnButtonDeleteAccount();
        deleteAccountPage.acceptEliminarAccountAlert(driver, wait);
        assertTrue(deleteAccountPage.isPageAccountLoaded(driver, wait));







    }
}
