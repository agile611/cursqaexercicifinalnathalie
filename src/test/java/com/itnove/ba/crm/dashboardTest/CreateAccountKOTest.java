package com.itnove.ba.crm.dashboardTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class CreateAccountKOTest extends BaseTest {

    @Test
    public void SuiteCreateTest() throws InterruptedException {
        String nameAccount = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        SuiteCreatePage suiteCreatePage = new SuiteCreatePage(driver);
        suiteCreatePage.clickOnButtonCreateAccount();
        assertTrue(suiteCreatePage.isCreateAccountLoaded(driver,wait));
        DontCreateAccountPage dontCreateAccountPage = new DontCreateAccountPage(driver);
        dontCreateAccountPage.setImputName (nameAccount);
        assertTrue(dontCreateAccountPage.isErrorPresent ());







    }
}
