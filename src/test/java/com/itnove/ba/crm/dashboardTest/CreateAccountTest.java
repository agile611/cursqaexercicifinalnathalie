package com.itnove.ba.crm.dashboardTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.CreateAccountPage;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import com.itnove.ba.crm.pages.SuiteCreatePage;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class CreateAccountTest extends BaseTest {

    @Test
    public void SuiteCreateTest() throws InterruptedException {
        String nameAccount = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        SuiteCreatePage suiteCreatePage = new SuiteCreatePage(driver);
        suiteCreatePage.clickOnButtonCreateAccount();
        assertTrue(suiteCreatePage.isCreateAccountLoaded(driver,wait));
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.setImputName(nameAccount, "http://sdfsdfjsdf.com/", "lalala@gmail.com",
                "Barcelona", "Barcelona", "Alcantara", "38530", "Spain");
        createAccountPage.setSearchAccount(nameAccount);
        Thread.sleep(6000);
        assertTrue(createAccountPage.isAccountRecentlyLoaded());





    }
}
