package com.itnove.ba.crm.dashboardTest;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import com.itnove.ba.crm.pages.SuiteCreatePage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SuiteCreateTest extends BaseTest {

    @Test
    public void SuiteCreateTest() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));
        SuiteCreatePage suiteCreatePage = new SuiteCreatePage(driver);

        suiteCreatePage.clickOnButtonCreateAccount();
        assertTrue(suiteCreatePage.isCreateAccountLoaded(driver,wait));

        suiteCreatePage.clickOnButtonCreateContact();
        assertTrue(suiteCreatePage.isCreateContactLoaded());

        suiteCreatePage.clickOnButtonCreateOpportunity();
        assertTrue(suiteCreatePage.isCreateOpportunityLoaded());

        suiteCreatePage.clickOnButtonCreateLead();
        assertTrue(suiteCreatePage.isCreateLeadLoaded());

        suiteCreatePage.clickOnButtonCreateDocument();
        assertTrue(suiteCreatePage.isCreateDocumentLoaded());

        suiteCreatePage.clickOnButtonCreateLogCall();
        assertTrue(suiteCreatePage.isCreateLogCallLoaded());

        suiteCreatePage.clickOnButtonCreateTask();
        assertTrue(suiteCreatePage.isCreatetaskLoaded());

        Thread.sleep(6000);







        //dashboardPage.hoverAndClickEveryCreate(driver, hover);
    }
}
